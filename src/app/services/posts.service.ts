import { Subject } from 'rxjs';
import { Post } from '../models/post.model';

export class PostsService {
  private posts: Post[] = [
    new Post('Ceci est le titre 1',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            0,
            new Date()),
    new Post('Ceci est le titre 2',
            'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
            0,
            new Date()),
    new Post('Ceci est le titre 3',
            'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.',
            0,
            new Date())
  ];
  postsSubject = new Subject<Post[]>();

  emitPosts() {
    this.postsSubject.next(this.posts.slice());
  }

  createNewPost(newPost: Post) {
    this.posts.push(newPost);
    this.emitPosts();
  }

  removePost(indexToRemove: number) {
    this.posts.splice(indexToRemove, 1);
    this.emitPosts();
  }

  addOneLoveIt(i: number) {
    this.posts[i].loveIts++;
    this.emitPosts();
  }

  removeOneLoveIt(i: number) {
    this.posts[i].loveIts--;
    this.emitPosts();
  }
}
